package ee.bcs.valiit;


// see on mingi jama.Tee uus.
public class Main {
    public static final String BIG_CONSTANT = "PLAHH!";
    public static void main (String[] args){
        Dog koer1 = new Dog();
        koer1.setName("Muki");
        koer1.setType("Mammal");
        Dog koer2 = new Dog();
        koer2.setName("Muri");
        koer2.setType("Bird");
        koer1.bark();
        koer2.bark();
        koer2.setName(koer1.getName());
        koer2.bark();
    }
}
